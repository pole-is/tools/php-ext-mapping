#!/usr/bin/env php
<?php
error_reporting(-1);

$cli_packages = [];
exec("apt-cache search php-cli | awk '/^php[0-9]\.[0-9]-/{print $1}'", $cli_packages, $error);
if ($error) {
    die("code: $error");
}
exec("apt-get install -yqq " . implode(" ", $cli_packages));

$versions = array_map(function($v)  {
    return substr($v, 3, 3);
}, $cli_packages);

echo "php_packages:\n";

foreach($versions as $version) {
    fputs(STDERR, "\nProcessing PHP ${version}\n");

    exec("apt-cache search ^php${version}- | awk '/^php${version}-/&&!/-dbg/{print$1}'", $packages, $error);
    if ($error) {
        die("code: $error");
    }

    $lines = [];
    exec("apt-cache show " . implode(" ", $packages) . " | egrep '^(Package|Provides):'", $lines, $error);
    if ($error) {
        die("code: $error");
    }

    $map = [];

    $package = null;
    foreach ($lines as $line) {
        [$header, $content] = explode(': ', $line, 2);
        switch ($header) {
            case 'Package':
                $package = $content;
                break;
            case 'Provides':
                if (!$package) {
                    fputs(STDERR, "ignoring Provides without Package: $line\n");
                    continue 2;
                }
                $thispackage = $package;
                $package = null;
                if (strpos($thispackage, "php${version}-") !== 0) {
                    continue 2;
                }
                $matches = [];
                preg_match_all("/\\bphp(?:\\Q${version}\\E)?\\-([^ ,]+)/", $content, $matches, PREG_PATTERN_ORDER);
                if (!$matches) {
                    continue 2;
                }
                $exts = array_unique($matches[1]);
                if (!$exts) {
                    continue 2;
                }
                foreach ($exts as $ext) {
                    $map[$ext] = $thispackage;
                }
                break;
            default:
                fputs(STDERR, "ignoring: $line\n");
        }
    }

    $builtins = [];
    exec("php${version} -n -m | egrep -v '^(\\[|$)'", $builtins, $error);
    if ($error) {
        die("code: $error");
    }

    foreach($builtins as $builtin) {
        $map[strtolower($builtin)] = 'builtin';
    }

    ksort($map);
    printf("  \"%s\":\n", $version);
    foreach ($map as $ext => $package) {
        printf("    %s: %s\n", $ext, $package);
    }
}
