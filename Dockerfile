ARG UPSTREAM=debian:buster
FROM ${UPSTREAM}

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -yqq

RUN . /etc/os-release \
&&  apt-get install -yq apt-transport-https ca-certificates curl \
&&  curl -sSL -o /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg \
&&  echo "deb https://packages.sury.org/php/ $VERSION_CODENAME main" > /etc/apt/sources.list.d/php.list \
&&  apt-get update -yqq

RUN apt-get install -yq php8.0-cli

COPY map-exts.php /usr/local/bin/map-exts.php

ENTRYPOINT ["/usr/local/bin/map-exts.php"]
