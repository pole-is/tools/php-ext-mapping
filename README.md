Ce projet permet de générer une cartographie des extensions et librairies disponibles dans le dépôt pour différentes versions de Debian, Ubuntu et PHP avec les [paquets packagés par Ondřej Surý](https://deb.sury.org/).

## Fonctionnement

Le projet crée une image Docker par OS et l'utilise les métadonnées d'APT pour extraire la correspondance entre les extensions PHP et les paquets dans lesquels elles sont packagées.

## CI

L'intégration continue est configurée pour générer un artefact contenant ces informations pour les 3 dernières versions de Debian et d'Ubuntu. L'archive est accessible depuis le [dernier job](https://gitlab.irstea.fr/pole-is/tools/php-ext-mapping/pipelines/latest?ref=master).
